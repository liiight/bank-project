Welcome to Or's bank project!

## Prerequisites:

- Python 2.7+
- [Git](https://git-scm.com/downloads)
- Pip (ships with Python)

## Installation:

1. Clone this repository: `git clone https://liiight@bitbucket.org/liiight/bank-project.git <TARGET DIR>`
2. From root dir of target run: `pip install -r requirements.txt`
3. Run `python run_webserver.py`
4. Load http://bank-example.com:5000/
5. When done, terminate with `Ctrl-C` to clean host file changes

**Note:** Host file should be altered programmatically, but if for any reason that fail, they need to be altered manually in order for the applications to work.
Insert the following to your host file if needed:
```
127.0.0.1 bank-example.com login-tracking.com
```

### Post process script:

- Run `python post_processor.py` to view results.
- Use `python post_processor.py --clear` flag to reset table

