from __future__ import print_function
import argparse

from apps.stats.stats_app import CollectedData, db
from terminaltables import SingleTable


def print_collected_data():
    data = CollectedData.query.all()
    header = ['Username', 'Number of attempts', 'Last attempt date']
    table_data = [header]
    for user in data:
        table_data.append([user.username, user.number_of_attempts, user.last_attempt.strftime('%Y-%m-%d %H:%M:%S')])
    print(SingleTable(table_data).table)


def erase_collected_data():
    data = CollectedData.query.delete()
    print('Deleting {} records from DB'.format(data))
    db.session.commit()


parser = argparse.ArgumentParser(description='List or clear collected data')
parser.add_argument('--list', action='store_true', default=True, help='Lists all collected data in DB')
parser.add_argument('--clear', action='store_true', help='Delete all collected data from DB')

if __name__ == "__main__":
    args = parser.parse_args()
    if args.clear:
        erase_collected_data()
    else:
        print_collected_data()
