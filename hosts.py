import logging

from python_hosts import Hosts, HostsEntry
from python_hosts.exception import UnableToWriteHosts

log = logging.getLogger(__name__)


def manage_hosts(hosts, action):
    try:
        host_file = Hosts()
    except AttributeError:
        print('HOSTS file is locked, cannot edit')
        return
    if action == 'add':
        entries = [HostsEntry(entry_type='ipv4', address=ip, names=host_names) for ip, host_names in hosts.items()]
        host_file.add(entries)
    else:
        for ip in hosts:
            host_file.remove_all_matching(address=ip)
    try:
        host_file.write()
    except UnableToWriteHosts:
        print('No permission to write to host file, please verify the entries are added to it manually')
