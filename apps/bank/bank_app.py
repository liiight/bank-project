import datetime
import logging
import os

import jwt
from flask import Flask, render_template, redirect, url_for, request, make_response
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import check_password_hash

ROOT_PATH = os.getcwd()
BASE_DB_PATH = 'apps/bank/db/users.db'
DB_PATH = os.path.join(ROOT_PATH, BASE_DB_PATH)

log = logging.getLogger(__name__)

app = Flask(__name__)
app.debug = True

app.config['SQLALCHEMY_DATABASE_URI'] = r'sqlite:///{}'.format(DB_PATH)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

SECRET = "shhh, it's a secret"


def create_token(user, expiration_hours=1):
    """
    Creates the JWT token

    :param user: Add username to token
    :param expiration_hours: Time in hour before expiration
    :return:  An encoded JWT token
    """
    time = datetime.datetime.utcnow() + datetime.timedelta(hours=expiration_hours)
    payload = {
        'exp': time,
        'username': user['username']
    }
    log.debug('Created token for user %s', user['username'])
    token = jwt.encode(payload, SECRET, algorithm='HS256')
    return token


def valid_token(token):
    """
    Tries to decode a JWT token, returns False on error, decoded token on success

    :param token: Received JWT token
    :return: False or decoded token
    """
    try:
        decoded_token = jwt.decode(token, SECRET, algorithms=['HS256'])
        log.debug('Succesfully decoded token')
    except jwt.DecodeError:
        log.warning('Could not correctly decode token or token expired')
        return False
    return decoded_token


class User(db.Model):
    """
    A DB schema representing a registered user
    """
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True)
    password = db.Column(db.String(80))
    created_at = db.Column(db.DateTime(), default=datetime.datetime.now())

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def __repr__(self):
        return '<User %r>' % self.username


def authenticated(username, password):
    """
    Queries DB if the username/password combination exists

    :param username: Received username
    :param password: Received password
    :return: True on successful authentication, False on fail
    """
    user = User.query.filter_by(username=username).first()
    if user and check_password_hash(user.password, password):
        log.debug('Authentication for user %s succeeded', user.username)
        return True
    log.warning('Authentication for username %s failed', username)
    return False


# Root path, redirects to login
@app.route('/')
def home():
    return redirect('/login')


# Account page path, responsible logout and verifies user session via cookie
@app.route('/account', methods=['GET', 'POST'])
def account():
    # Logout - Redirects to login page and reset token cookie
    if request.method == 'POST' and request.form['logout']:
        response = make_response(redirect(url_for('login')))
        response.set_cookie('token', '')
        return response

    # Checks for valid token existence, redirects to login page if it fails
    token = request.cookies.get('token')
    if not token or not valid_token(token):
        error = 'Token expired or user not logged in'
        return redirect(url_for('login', error=error))

    # Verifies token is valid and redirects user to account page
    token = valid_token(token)
    username = token['username']
    return render_template('account.html', username=username)


# Login page, creates session token on successful authentication
@app.route('/login', methods=['GET', 'POST'])
def login():
    error = request.args.get('error') or None
    if request.method == 'POST':
        if not authenticated(request.form['username'], request.form['password']):
            error = 'Invalid Credentials. Please try again.'
        else:
            user = {'username': request.form['username']}
            response = make_response(redirect(url_for('account')))
            response.set_cookie('token', create_token(user))
            return response
    return render_template('login.html', error=error)
