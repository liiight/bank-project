import datetime
import logging
import os

from flask import Flask, request, render_template
from flask.helpers import send_from_directory
from flask_sqlalchemy import SQLAlchemy

log = logging.getLogger(__name__)

app = Flask(__name__)
app.debug = True

root_path = os.getcwd()
base_db_path = 'apps/stats/db/collection.db'
db_path = os.path.join(root_path, base_db_path)

app.config['SQLALCHEMY_DATABASE_URI'] = r'sqlite:///{}'.format(db_path)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class CollectedData(db.Model):
    """
    A schema representing a collected login attempt
    """
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True)
    number_of_attempts = db.Column(db.Integer, default=0)
    last_attempt = db.Column(db.DateTime(), default=datetime.datetime.now())
    created_at = db.Column(db.DateTime(), default=datetime.datetime.now())

    def __init__(self, username):
        self.username = username

    def __repr__(self):
        return '<CollectedData %r>' % self.username

# Serves requests to fetch relvant JS files
@app.route('/js/<path:path>')
def send_js(path):
    log.info('received request to serve {} file'.format(path))
    return send_from_directory('static', path)


# Responsible for setting and fetching collected credentials
@app.route('/collection/', methods=['GET', 'POST'])
def post_credentials():
    if request.method == 'POST' and request.form['username']:
        username = request.form['username']
        log.info('saving login attempt for {}'.format(username))
        _save_credentials_to_db(username)
        return 'Success'
    else:
        users = CollectedData.query.all()
        return render_template('stats.html', users=users)

# Saves Credentials to DB
def _save_credentials_to_db(username):
    data = CollectedData.query.filter_by(username=username).first()
    if not data:
        data = CollectedData(username)
        db.session.add(data)
        db.session.commit()
    data.number_of_attempts += 1
    db.session.add(data)
    db.session.commit()
