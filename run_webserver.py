import logging

import cherrypy
from paste.translogger import TransLogger

from apps.bank.bank_app import app as bank_app
from apps.stats.stats_app import app as stats_app
from hosts import manage_hosts

BANK_DOMAIN = 'bank-example.com'
TRACKING_DOMAIN = 'login-tracking.com'
PORT = 5000

HOSTS = {'127.0.0.1': [BANK_DOMAIN, TRACKING_DOMAIN]}

log = logging.getLogger(__name__)


def run_server():
    # Enable WSGI access logging via Paste
    bank_app_logged = TransLogger(bank_app)
    stats_app_logged = TransLogger(stats_app)

    vhost = cherrypy.wsgi.VirtualHost(None,
                                      domains={
                                          '{}:{}'.format(BANK_DOMAIN, PORT): bank_app_logged,
                                          '{}:{}'.format(TRACKING_DOMAIN, PORT): stats_app_logged,
                                      }
                                      )
    # Mount the WSGI callable object (app) on the root directory
    cherrypy.tree.graft(vhost)

    # Set the configuration of the web server
    cherrypy.config.update({
        'engine.autoreload_on': True,
        'log.screen': True,
        'server.socket_port': PORT,
        'server.socket_host': '0.0.0.0'
    })

    # Start the CherryPy WSGI web server
    log.info('Starting web server')
    cherrypy.engine.start()
    cherrypy.engine.block()


if __name__ == "__main__":
    try:
        logging.basicConfig(filename='operation.log', level=logging.DEBUG)
        manage_hosts(HOSTS, 'add')
        run_server()
    except KeyboardInterrupt:
        log.info('Shutting down')
        manage_hosts(HOSTS, 'remove')
